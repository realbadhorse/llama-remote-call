#include "LRC.h"
#include <corecrt_terminate.h>
#include <stdint.h>
#include <stdio.h>

//#define _LRC_DEBUG

#ifdef _LRC_DEBUG
#define LOG(...) std::printf(__VA_ARGS__) 
#else 
#define LOG(...) (0) 
#endif //  

#define LOG_FNAME() LOG("\t--%s--\n", __func__)

#define PAGE_SHIFT      12
#define PAGE_SIZE       (1ULL << PAGE_SHIFT)
#define PAGE_MASK       (~(PAGE_SIZE-1))
#define PAGE_ALIGN_LOW(addr) (addr & PAGE_MASK)

#define PAGE_FLAG_FETCH (1 << 0)
#define PAGE_FLAG_WRITE (1 << 1)

#define LVL0_RET_MAGIC 0xB00B1E5

const uint64_t STACK_SIZE = PAGE_SIZE;
const uint64_t STACK_MAP = 0x10000; //already aligned
const uint64_t STACK_START = STACK_MAP + STACK_SIZE - 50; //extra room on stack just in case


inline static void get_uc_err(uc_err err, uint64_t lineno) {
#ifdef _LRC_DEBUG
    LOG_FNAME();
    if (err != 0) [[unlikely]] {
        LOG("[Line: %llu] Failed on with error returned %u: %s\n",
            lineno, err, uc_strerror(err));
    }
#endif
}

static void dump_maps(LRC* lrc) {
#ifdef _LRC_DEBUG
    LOG_FNAME();
    LOG("\ncode_pages map: eVA <-> hVA  (flags)\n");
    for (auto& p : lrc->code_page_mappings) {
        auto eva = p.first;
        auto mem_record = p.second;
        auto hva = mem_record.hva_address;
        auto flags = mem_record.flags;

        LOG("0x%llx <-> 0x%p  (%u)\n", eva, hva, flags);
    }
    LOG("\n");
#endif
}

inline static bool is_emu_exit(uc_engine* uc, uint64_t addr, LRC* lrc)
{
    LOG_FNAME();
    if (addr == LVL0_RET_MAGIC) [[unlikely]] {
        LOG("Encountered level 0 return. Ending emulation.\n");
        dump_maps(lrc);
        uc_emu_stop(uc);
        return true;
    }
    return false;
}

static void hook_intr(uc_engine* uc, void* user_data) {
    LOG("REACHED SYSCALL. KILLING EMU\n");
    uc_emu_stop(uc);
}

static void hook_code_trace(uc_engine* uc, uint64_t address, uint32_t size, LRC* lrc)
{
    LOG_FNAME();
    uint64_t rip{};

    if (lrc->bitness == LRC_X64) {
        auto err = uc_reg_read(uc, UC_X86_REG_RIP, &rip);
        get_uc_err(err, __LINE__);
    }
    else if (lrc->bitness == LRC_X86) {
        auto err = uc_reg_read(uc, UC_X86_REG_EIP, &rip);
        get_uc_err(err, __LINE__);
    }

    LOG(">>> Inst at hVA 0x%llx, eRIP %llx, size = 0x%x ---", address, rip, size);
    LOG("\t");
    for (int i = 0; i < size; i++) {
        unsigned char buf;
        uc_mem_read(uc, address + i, &buf, 1);
        LOG("%.2hhx ", buf);
    }
    LOG("\n");
}


static void hook_page_write_fetch(uc_engine* uc, uc_mem_type type,
    uint64_t addr, int size, int64_t value, LRC* lrc)
{
    LOG_FNAME();
    LOG("Entered handle_page_write for eVA 0x%llx\n", addr);

    if (lrc->no_cache_writes) {
        /* write all modified data from back to process asap, dont wait for execution to finish
        probably helps against overwriting surrounding data if write pages are too volatile 
        and emu execution time is too long at the cost of slower emu speed due to calling flush continuously */
        lrc->writeback_and_flush(true, true); 
    }

    auto aligned_addr = PAGE_ALIGN_LOW(addr);
    //should never happen
    if (!lrc->code_page_mappings.contains(aligned_addr)) {
        LOG("writing to a page not mapped by us.\n");
        std::terminate();
    }

    auto& map_record = lrc->code_page_mappings[aligned_addr];
     
    if (type == UC_MEM_WRITE) {
        map_record.flags |= PAGE_FLAG_WRITE;
    }
    else if (type == UC_MEM_FETCH) {
        map_record.flags |= PAGE_FLAG_FETCH;
    }

    auto err = uc_hook_del(uc, map_record.hhandle);
    get_uc_err(err, __LINE__);


}

static void handle_mapping_needed(uc_engine* uc, uint64_t addr, LRC* lrc) {

    LOG_FNAME();
    uc_hook hook_handle_write{};
    uc_err err{};

    LOG("Mapping needed at eVA 0x%llx\n", addr);

    auto buffer = malloc(PAGE_SIZE);
    if (buffer == nullptr) [[unlikely]] {
        LOG("malloc error: out of memory! LINE %u\n", __LINE__);
        return;
    }

    const auto aligned_addr = PAGE_ALIGN_LOW(addr);

    if (!lrc->read_fn(aligned_addr, buffer, PAGE_SIZE)) [[unlikely]] {
        LOG("read_fn failed while fetching at LINE %u\n", __LINE__);
        free(buffer);
        return;
    }

    //map the needed range
    err = uc_mem_map_ptr(uc, aligned_addr, PAGE_SIZE, UC_PROT_ALL, buffer);

    //add hook to mark it if it gets written/fetched. FETCH does not trigger
    err = uc_hook_add(uc, &hook_handle_write, UC_HOOK_MEM_WRITE | UC_HOOK_MEM_FETCH,
        hook_page_write_fetch, lrc, aligned_addr, aligned_addr + PAGE_SIZE);
    get_uc_err(err, __LINE__);

    auto map_record = LRC::emu_mem_map_record{
        .hva_address = buffer,
        .hhandle = hook_handle_write,
        .flags = 0
    };
    lrc->code_page_mappings.insert(std::make_pair(aligned_addr, map_record));

    LOG("Mapped code page at eVA 0x%llx, hVA 0x%p\n", aligned_addr, buffer);
    return;
}


auto hook_unmapped_mem(uc_engine* uc, uc_mem_type type,
    uint64_t addr, int size, int64_t value, LRC* lrc) -> bool
{
    LOG_FNAME();
    /*
    addr is eVA where event occured. ex. It tried to read at eVA addr but it isnt mapped
    RIP is eVA where instruction is that is tring to perform read/write/exec
    */
    //emul has already ended
    if (is_emu_exit(uc, addr, lrc)) [[unlikely]] {
        return false; //uni will still run remaining hooks lmao
    }

#ifdef _LRC_DEBUG
    LOG("\n--- Entered hook_unmapped_mem ---\n");
    LOG(
        "\tuc_mem_type: %u\n"
        "\taddr: 0x%llx\n"
        "\tsize: %u\n"
        "\tvalue: %llu\n\n",
        type, addr, size, value
    );

    LOG("addr (0x%llx):\n\t", addr);
    for (int i = 0; i < 0x10; i++) {
        unsigned char tmp;
        if(lrc->read_fn(addr + i, &tmp, 1)){
            LOG("%.2hhx ", tmp);
        }
        else{
            LOG("Error printing bytes from addr");
        }
    }

    uint64_t ctx_rip{};
    if (lrc->bitness == LRC_X64) {
        uc_reg_read(uc, UC_X86_REG_RIP, &ctx_rip);
    } else if (lrc->bitness == LRC_X86) {
        uc_reg_read(uc, UC_X86_REG_EIP, &ctx_rip);
    }

    LOG("RIP: 0x%llx\n\t", ctx_rip);

    for (int i = 0; i < 0x10; i++) {
        unsigned char tmp;
        if(lrc->read_fn(ctx_rip + i, &tmp, 1))
        {
            LOG("%.2hhx ", tmp);
        } else{
            LOG("Error printing bytes from RIP");
        }
    }
    LOG("\n\n");

    dump_maps(lrc);
#endif

    handle_mapping_needed(uc, addr, lrc);

    return true;
}


void LRC::stack_reset() {
    LOG_FNAME();
    uc_err err{};

    if (bitness == LRC_X64) {
        err = uc_reg_write(uc, UC_X86_REG_RSP, &STACK_START);
        err = uc_reg_write(uc, UC_X86_REG_RBP, &STACK_START);
    }
    else if (bitness == LRC_X86) {
        err = uc_reg_write(uc, UC_X86_REG_ESP, &STACK_START);
        err = uc_reg_write(uc, UC_X86_REG_EBP, &STACK_START);
    }
}

void LRC::writeback_and_flush(bool writeback, bool only_flush_write) {

    LOG_FNAME();
    for (auto& p : this->code_page_mappings) {
        auto eVA = p.first;
        auto map_record = p.second;
        auto hVA = map_record.hva_address;
        auto isdirty = map_record.flags & PAGE_FLAG_WRITE;
        auto isfetch = map_record.flags & PAGE_FLAG_FETCH;
        if (hVA) [[likely]] {
            //wb if dirty
            if (writeback && isdirty) {
                if (!this->write_fn(eVA, hVA, PAGE_SIZE)) {
                    LOG("Failed writeback on eVA 0x%llx, hVA 0x%p\n", eVA, hVA);
                }
            }
        //remove the page since its just data
        if (!isfetch && !only_flush_write) {
            uc_mem_unmap(this->uc, eVA, PAGE_SIZE);
            free(hVA);
            this->code_page_mappings.erase(eVA);
        }
        }
        else {
            LOG("hVA is null. Something is fucked.\n");
            //std::terminate();
        }
    }
    LOG("Cleanup complete\n");
}

/*address of entrypoint in remote process*/
uint64_t LRC::do_call(uint64_t remote_start_address_, LRC_context* context_) {

    LOG_FNAME();
    uc_err err;

    //put dummy retval on stack
    if (this->bitness == LRC_X64) {
        stack_push(uint64_t{ LVL0_RET_MAGIC });
    } else if (this->bitness == LRC_X86) {
        stack_push(uint32_t{ LVL0_RET_MAGIC });
    }

    //setup ctx
    if (context_) {
        switch (bitness) {
        case LRC_X64:
            //uc_reg_write(uc, UC_X86_REG_RAX, &context_->RAX);
            //uc_reg_write(uc, UC_X86_REG_RBX, &context_->RBX);
            uc_reg_write(uc, UC_X86_REG_RCX, &context_->RCX);
            uc_reg_write(uc, UC_X86_REG_RDX, &context_->RDX);
            //uc_reg_write(uc, UC_X86_REG_RSI, &context_->RSI);
            //uc_reg_write(uc, UC_X86_REG_RDI, &context_->RDI);
            //uc_reg_write(uc, UC_X86_REG_RBP, &context_->RBP);
            //uc_reg_write(uc, UC_X86_REG_RSP, &context_->RSP);
            uc_reg_write(uc, UC_X86_REG_R8, &context_->R8);
            uc_reg_write(uc, UC_X86_REG_R9, &context_->R9);
            //uc_reg_write(uc, UC_X86_REG_R10, &context_->R10);
            //uc_reg_write(uc, UC_X86_REG_R11, &context_->R11);
            //uc_reg_write(uc, UC_X86_REG_R12, &context_->R12);
            //uc_reg_write(uc, UC_X86_REG_R13, &context_->R13);
            //uc_reg_write(uc, UC_X86_REG_R14, &context_->R14);
            //uc_reg_write(uc, UC_X86_REG_R15, &context_->R15);
            break;

        case LRC_X86:
            //uc_reg_write(uc, UC_X86_REG_EAX, &context_->RAX);
            //uc_reg_write(uc, UC_X86_REG_EBX, &context_->RBX);
            uc_reg_write(uc, UC_X86_REG_ECX, &context_->RCX);
            uc_reg_write(uc, UC_X86_REG_EDX, &context_->RDX);
            //uc_reg_write(uc, UC_X86_REG_ESI, &context_->RSI);
            //uc_reg_write(uc, UC_X86_REG_EDI, &context_->RDI);
            //uc_reg_write(uc, UC_X86_REG_EBP, &context_->RBP);
            //uc_reg_write(uc, UC_X86_REG_ESP, &context_->RSP);
            break;
        }
    }

    LOG("Mapping initial code page\n");
    if (!this->code_page_mappings.contains(remote_start_address_))
        handle_mapping_needed(uc, remote_start_address_, this);

    //start emu
    //remote_start_address_ is entrypoint of code because remote and emulated AS are 1:1
    err = uc_emu_start(uc, remote_start_address_, 0, 0, 0); //emulate in infinite time,
    if (err != 0) {
        LOG("Failed on uc_emu_start() with error returned %u: %s\n",
            err, uc_strerror(err));
    }

    writeback_and_flush(true);

    stack_reset();

    uint64_t ret{};
    if (bitness == LRC_X64) {
        uc_reg_read(uc, UC_X86_REG_RAX, &ret);
    } else if (bitness == LRC_X86) {
        uc_reg_read(uc, UC_X86_REG_EAX, &ret);
    }

    return ret;

}


LRC::LRC(rwfunc read_, rwfunc write_, LRC_BITNESS bitness_)
{
    read_fn = read_;
    write_fn = write_;
    bitness = bitness_;

    uc_hook mapping_needed_hnd;
    uc_hook hook_syscall_hnd;
    uc_err err{};


    switch (bitness) {
    case LRC_X86:
        err = uc_open(UC_ARCH_X86, UC_MODE_32, &this->uc);
        break;
    case LRC_X64:
        err = uc_open(UC_ARCH_X86, UC_MODE_64, &this->uc);
        break;
    default:
        LOG("ERROR: unknown arch\n");
    }

    if (err) {
        LOG("ERROR: initializing uc_open\n");
        //std::terminate();
    }

    //setup hooks to manage events
    err = uc_hook_add(uc, &mapping_needed_hnd, UC_HOOK_MEM_UNMAPPED, hook_unmapped_mem, this, 1, 0);
    err = uc_hook_add(uc, &hook_syscall_hnd, UC_HOOK_INTR, hook_intr, this, 1, 0);
#ifdef _LRC_DEBUG
    uc_hook trace2{};
    err = uc_hook_add(uc, &trace2, UC_HOOK_CODE, hook_code_trace, this, 1, 0);
#endif

    //stack mapping
    stack = std::make_unique<char[]>(STACK_SIZE);
    uc_mem_map_ptr(this->uc, STACK_MAP, STACK_SIZE, UC_PROT_ALL, this->stack.get());

    stack_reset();
}

LRC::~LRC() {
    //todo
    // 
    //free all code maps, at this point it should only be the fetch pages
    for (auto& p : code_page_mappings) {
        auto eVA = p.first;
        auto map_record = p.second;
        auto hVA = map_record.hva_address;
        if (!hVA) [[unlikely]] {
            LOG("hVA is NULL. PANIC\n");
            //std::terminate();
        }
        free(hVA);
        code_page_mappings.erase(code_page_mappings.find(eVA));
    }
    LOG("Object cleanup complete\n");
    uc_close(uc);
}