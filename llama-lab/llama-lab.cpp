#include "LRC.h"
#include <iostream>
#include <chrono>
#include <processthreadsapi.h>
#include <Psapi.h>
#include <tlhelp32.h>
#include <string>
#include <cwchar>

volatile auto lmao_data = 50;

auto emul_target() -> int
{
    //volatile int x = 10;
    //int result = x + lmao_data + (int)GetCurrentProcess();
    lmao_data += 1;
    return 0;
}

//auto read(uint64_t address, void* buffer, unsigned int size) -> bool
//{
//    memcpy(buffer, (void*)address, size);
//    return true;
//}
//
//auto write(uint64_t address, void* buffer, unsigned int size) -> bool
//{
//    memcpy((void*)address, buffer, size);
//    return true;
//}


HANDLE GetProcessHandleByID(int nID)//Get process handle by process ID
{
    return OpenProcess(PROCESS_ALL_ACCESS, TRUE, nID);
}
 
DWORD GetProcessIDByName(const WCHAR* pName)
{
    HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    if (INVALID_HANDLE_VALUE == hSnapshot) {
        return NULL;
    }
    PROCESSENTRY32 pe = { sizeof(pe) };
    for (BOOL ret = Process32First(hSnapshot, &pe); ret; ret = Process32Next(hSnapshot, &pe)) {
        if (!wcscmp(pe.szExeFile, pName)) {
            CloseHandle(hSnapshot);
            return pe.th32ProcessID;
        }
        //printf("%-6d %s\n", pe.th32ProcessID, pe.szExeFile);
    }
    CloseHandle(hSnapshot);
    return 0;
}

uintptr_t GetModule(int pid, const wchar_t* name)
{
    MODULEENTRY32 me32;
    me32.dwSize = sizeof(MODULEENTRY32);
    HANDLE hDump = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE | TH32CS_SNAPMODULE32, pid);
    if (Module32First(hDump, &me32))
    {
        do {
            printf("-> %ls\n", me32.szModule);
            if (!wcscmp(me32.szModule, name))
            {
                CloseHandle(hDump);
                return (uintptr_t)me32.modBaseAddr;
                break;//dumb code
            }
        } while (Module32Next(hDump, &me32));
    }
    return 0;
}

HANDLE g_hnd{};

const unsigned int client_ci_off = 0x944EA0;
const unsigned int client_str_off = 0x00B609A0;

auto main() -> int
{

    auto pid = GetProcessIDByName(L"emul-dummy.exe");
    g_hnd = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
    auto base = GetModule(pid, L"emul-dummy.exe");
    //auto client = GetModule(pid, L"client.dll");
    //printf("base %llx\n", base);
    //printf("client %llx\n", client);

    auto read = [](uint64_t addr, void* buffer, int size) -> bool {
        size_t bytes_read = 0;
        auto r = !!ReadProcessMemory(g_hnd, (PVOID)addr, buffer, size, &bytes_read);
        return r;
    };

    auto write = [](uint64_t addr, void* buffer, int size) -> bool {
        size_t bytes_read = 0;
        auto r = !!WriteProcessMemory(g_hnd, (PVOID)addr, buffer, size, &bytes_read);
        return r;
    };

    LRC lrc(read, write, LRC_X64);
    auto t1 = std::chrono::high_resolution_clock::now();
    //lrc.stack_push(uint32_t{ 0x1000 + 0x400 }); //return code* (stack region)
    //lrc.stack_push( client + client_str_off ); //pointer to interface name string "VClient018"
    uint64_t r{};
    for(int i = 0; i < 1000; i++)
        r = lrc.do_call(base + 0x1060);
    auto t2 = std::chrono::high_resolution_clock::now();
    printf("took: %fms\n", std::chrono::duration<double, std::milli>(t2 - t1).count());
    printf("returned 0x%llx\n", r);
    
    // uint64_t ret;

    // auto t1 = std::chrono::high_resolution_clock::now();
    // for (int i = 0; i < 1000; i++) {
    //     lrc.do_call(reinterpret_cast<uint64_t>(&emul_target));
    //     lrc.stack_reset();
    //     //printf("lmaodata: %u\n", lmao_data);
    // }
    // auto t2 = std::chrono::high_resolution_clock::now();
    // auto ms_int = duration_cast<std::chrono::milliseconds>(t2 - t1);
    // printf(
    //     "Fin: %u Took: %lldms\n",
    //     lmao_data, ms_int.count()
    // );
    getchar();
}
